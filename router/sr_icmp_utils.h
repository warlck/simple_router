
#ifndef SR_ICMP_UTILS_H
#define SR_ICMP_UTILS_H



#include "sr_router.h"


/* Utils for working with ICMP datagrams */
sr_icmp_t3_hdr_t  *icmp_hdr_from_eth(uint32_t *buf);
int icmp_cksum_is_correct(uint8_t  *);
void sr_icmp_for_us(struct sr_instance * sr, uint8_t * buf, char *interface);
void sr_send_icmp_port_unreachable(struct sr_instance * sr, uint8_t * buf,
                                   char *interface);
void sr_send_icmp_net_unreachable(struct sr_instance * sr, uint8_t * buf,
                                   char *interface);
void sr_send_icmp_host_unreachable(struct sr_instance * sr, uint8_t * buf,
                                   char *interface);
void sr_send_icmp_time_exceed(struct sr_instance * sr, uint8_t * buf,
                              char *interface);





#endif