#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sr_ip_utils.h"
#include "sr_eth_utils.h"
#include "sr_protocol.h"
#include "sr_utils.h"
#include "sr_if.h"
#include "sr_lpm.h"
#include "sr_rt.h"
#include "sr_router.h"

static void decrement_ttl(sr_ip_hdr_t *iphdr);
static int ttl_exceeded(struct sr_instance * sr, uint8_t * buf, char *interface);


void
sr_handle_ip(struct sr_instance * sr,
             uint8_t * buf,
             unsigned int len,
             char* interface)
{
  int minlen = sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t);
  if (len < minlen) {
    return;
  }

  if (!ip_cksum_is_correct(buf)) {
    return;
  }


  sr_ip_hdr_t *iphdr = ip_hdr_from_eth(buf);

  if (iphdr->ip_v != IP_VERSION_4) {
    return;
  }

  /* Need to check if IP packet is addressed to any of  router's
    interfaces. If it is addessed to router */
  if (ip_pkt_addressed_to_us(sr, iphdr)) {
    sr_ip_pkt_for_us(sr, buf, len, interface);
  } else {
     sr_ip_pkt_not_for_us(sr, buf, len, interface); 
  }

}

void
sr_ip_pkt_for_us(struct sr_instance * sr,
                 uint8_t * buf,
                 unsigned int len,
                 char* interface)
{
  sr_ip_hdr_t *iphdr = ip_hdr_from_eth(buf);

  /* IP packet is of ICMP protocol */
  if (ip_protocol(iphdr) == ip_protocol_icmp) {
    sr_icmp_for_us(sr, buf, interface);
  } else {
    sr_send_icmp_port_unreachable(sr, buf, interface);
  }
}


void
sr_ip_pkt_not_for_us(struct sr_instance *sr,
                     uint8_t * buf,
                     unsigned int len,
                     char* interface)
{
  sr_ip_hdr_t *iphdr = ip_hdr_from_eth(buf);
  decrement_ttl(iphdr);

  /* Has side effect of sending ICMP time exceeded message
     if newly decremented ttl is 0. */
  if (ttl_exceeded(sr, buf, interface)) {
    return;
  }

  struct sr_rt *lpm_rt_entry = sr_lpm(sr, buf);
  if (lpm_rt_entry == NULL) {
    /* Did not find IP address in routing table, 
       need to send ICMP net ureachable */
    sr_send_icmp_net_unreachable(sr, buf, interface);
    return;
  }

  uint32_t target_ip = lpm_rt_entry->gw.s_addr;
  /* Checks if there is already and ARP entry for the gateway address, we need
     need to send packet to. */
  struct sr_arpentry *entry = sr_arpcache_lookup(&sr->cache, target_ip); 

  if (entry == NULL) {
    /* Send ARP request to interface of lmp_rt_entry and queue the packet */
    struct sr_arpreq *req = sr_arpcache_queuereq(&sr->cache, target_ip, buf, 
      len, lpm_rt_entry->interface);

    sr_handle_arpreq(sr, req);
  } else {
    /* Send eth frame */
    struct sr_if *iface = sr_get_interface(sr, lpm_rt_entry->interface);
    uint8_t *ether_saddr = iface->addr;
    uint8_t *ether_daddr = entry->mac;

    sr_update_eth_addresses(buf, ether_saddr, ether_daddr);
    sr_send_packet(sr, buf, len, lpm_rt_entry->interface);
  }


}


sr_ip_hdr_t *
ip_hdr_from_eth(uint8_t * buf)
{
  return (buf + sizeof(sr_ethernet_hdr_t));
}



int ip_cksum_is_correct(uint8_t *buf) {
  int res = 0;
  sr_ip_hdr_t *iphdr = ip_hdr_from_eth(buf);

  /* Get the length of the ICMP message and included data by referring to
     total length field of IP datagram.  */
  size_t len = sizeof(sr_ip_hdr_t);

  uint16_t ip_sum = iphdr->ip_sum;
  iphdr->ip_sum = 0;

  if (cksum(iphdr, len) == ip_sum) {
    res = 1;
  }

  return res;
}

int
ip_pkt_addressed_to_us(struct sr_instance* sr, sr_ip_hdr_t *iphdr)
{
  int res = 0;
  uint32_t ip_dst = iphdr->ip_dst;
  struct sr_if* if_walker = 0;

  if_walker = sr->if_list;
  while (if_walker)
  {
    if (ip_dst == if_walker->ip)
    {
      res = 1;
      break;
    }
    if_walker = if_walker->next;
  }
  return res;
}


size_t
ip_hdr_len_with_data(size_t data_len)
{
  return data_len + sizeof(sr_ip_hdr_t);
}



sr_ip_hdr_t *
make_icmp_ip_datagram(uint32_t ip_src, uint32_t ip_dst, size_t len,
                      uint8_t *data, uint16_t *ip_id)
{
  size_t hdr_len = ip_hdr_len_with_data(len);
  sr_ip_hdr_t *iphdr = malloc(hdr_len);

  if (iphdr == NULL) {
    return NULL;
  }

  /* Increment ip_id of router before passing to ip header */
  *ip_id += 1;

  /* Fill in the IP header values, ip_dst and ip_src are passed in network
     order.   */
  iphdr->ip_hl = IP_HL;
  iphdr->ip_v = IP_VERSION_4;
  iphdr->ip_tos = 0;
  iphdr->ip_len = htons(hdr_len);
  iphdr->ip_id = htons(*ip_id);
  iphdr->ip_off = htons(IP_DF);
  iphdr->ip_ttl = DEFAULT_IP_TTL;
  iphdr->ip_p = ip_protocol_icmp;
  iphdr->ip_sum = 0;
  iphdr->ip_src = ip_src;
  iphdr->ip_dst = ip_dst;

  /* Calculate checksum */
  iphdr->ip_sum = cksum(iphdr, hdr_len);
  /* Copy passed data */
  memcpy((uint8_t *)iphdr + sizeof(sr_ip_hdr_t), data, len);

  return iphdr;
}



uint8_t *
new_ip_eth_frame(uint8_t *buf, sr_ip_hdr_t *ip_datagram)
{
  sr_ethernet_hdr_t *eth_hdr = (sr_ethernet_hdr_t *)buf;
  uint16_t ether_type = htons(ethertype_ip);
  size_t len = ntohs(ip_datagram->ip_len);

  uint8_t *new_eth_hdr = make_eth_frame(eth_hdr->ether_shost,
                                        eth_hdr->ether_dhost,
                                        ether_type, ip_datagram, len);

  return new_eth_hdr;
}

static void
decrement_ttl(sr_ip_hdr_t *iphdr)
{
  iphdr->ip_ttl -= 1;
  iphdr->ip_sum = 0;
  iphdr->ip_sum = cksum(iphdr, sizeof(sr_ip_hdr_t));
}

static int
ttl_exceeded(struct sr_instance * sr, uint8_t * buf, char *interface)
{
  int res = 1;
  sr_ip_hdr_t *iphdr = ip_hdr_from_eth(buf);
  if (iphdr->ip_ttl > 0) {
    res = 0;
    return res;
  }

  sr_send_icmp_time_exceed(sr, buf, interface);
  return res;
}

