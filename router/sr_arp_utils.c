#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "sr_eth_utils.h"
#include "sr_protocol.h"
#include "sr_utils.h"
#include "sr_if.h"
#include "sr_arp_utils.h"
#include "sr_rt.h"

static sr_arp_hdr_t make_arp_hdr_host_copy(uint8_t *buf);
static sr_arp_hdr_t new_arp_rep_hdr(struct sr_instance*, sr_arp_hdr_t *, char *);
static sr_arp_hdr_t new_arp_req_hdr(struct sr_instance * sr, uint32_t tip,
                                    char *interface);
static uint8_t *new_arp_eth_frame(sr_arp_hdr_t *);
static void make_network_arp_hdr(unsigned short , char *, char *, uint32_t,
                                 uint32_t, sr_arp_hdr_t *);


void
sr_handle_arp(struct sr_instance * sr,
              uint8_t * buf,
              unsigned int len,
              char* interface)
{
  int minlen = sizeof(sr_ethernet_hdr_t) + sizeof(sr_arp_hdr_t);
  if (len < minlen) {
    return;
  }

  /* Original ARP header in network order */
  sr_arp_hdr_t *arp_hdr_n = (sr_arp_hdr_t *)(buf + sizeof(sr_ethernet_hdr_t));

  /* ARP header in host order */
  sr_arp_hdr_t arp_hdr_h = make_arp_hdr_host_copy(arp_hdr_n);

  /* Return if ARP request's hardware address is not for Ethernet Address.
    Protocol field of ARP packet is always for IP, so no need to check. */
  if (arp_hdr_h.ar_hrd != 1) {
    return;
  }
  /* Get the opcode */
  unsigned short opcode = arp_hdr_h.ar_op;

  /* Received ARP request */
  if (opcode == arp_op_request)  {
    sr_handle_arp_request(sr, arp_hdr_n, interface);
  } else if (opcode == arp_op_reply) {
    sr_handle_arp_reply(sr, arp_hdr_n, interface);
  }

}


/* Takes care of the ARP request by inserting MAC->IP mapping into arpchache,
   and preparing and replying with ARP response. */
void
sr_handle_arp_request(struct sr_instance * sr,
                      sr_arp_hdr_t *arp_hdr,
                      char *interface)
{

  sr_arpcache_insert(&sr->cache, arp_hdr->ar_sha, arp_hdr->ar_sip);
  sr_arp_hdr_t arp_response = new_arp_rep_hdr(sr, arp_hdr, interface);
  /* The frame is dynamically allocated by us. Need to free the frame buffer
     memory after sending it on the wire. */
  uint8_t *arp_eth_frame = new_arp_eth_frame(&arp_response);
  if (arp_eth_frame == NULL) {
    return;
  } else {
    sr_send_packet(sr, arp_eth_frame, framelen(sizeof(sr_arp_hdr_t)), interface);
    free(arp_eth_frame);
  }

}


/* Takes care of the ARP reply. When ARP relpy received, caches the MAC->IP
    mapping, goes through the request queue and sends outstanding packets.  */
void
sr_handle_arp_reply(struct sr_instance * sr,
                    sr_arp_hdr_t *arp_hdr,
                    char *interface)
{
  struct sr_arpreq *req = sr_arpcache_insert(&sr->cache, arp_hdr->ar_sha, arp_hdr->ar_sip);

  if (req) {
    struct sr_packet *pkt = req->packets;

    while (pkt) {
      struct sr_if *iface = sr_get_interface(sr, pkt->iface);
      uint8_t *ether_saddr = iface->addr;
      uint8_t *ether_daddr = arp_hdr->ar_sha;
      sr_update_eth_addresses(pkt->buf, ether_saddr, ether_daddr);
      sr_send_packet(sr, pkt->buf, pkt->len, pkt->iface);
      pkt = pkt->next;
    }
    sr_arpreq_destroy(&sr->cache, req);
  }
}



void
sr_send_arp_request(struct sr_instance *sr, uint32_t tip, char *interface) {
  sr_arp_hdr_t arp_request = new_arp_req_hdr(sr, tip, interface);
  uint8_t *arp_eth_frame = new_arp_eth_frame(&arp_request);

  if (arp_eth_frame == NULL) {
    return;
  } else {
    sr_send_packet(sr, arp_eth_frame, framelen(sizeof(sr_arp_hdr_t)), interface);
    free(arp_eth_frame);
  }

}




/* Creates ARP response packet */
static sr_arp_hdr_t
new_arp_rep_hdr(struct sr_instance * sr,
                sr_arp_hdr_t *arp_hdr,
                char *interface)
{
  sr_arp_hdr_t result;
  unsigned short  opcode = arp_op_reply;
  struct sr_if *iface = sr_get_interface(sr, interface);

  char *sha = iface->addr;
  char *tha = arp_hdr->ar_sha;
  uint32_t sip = iface->ip;
  uint32_t tip = arp_hdr->ar_sip;
  make_network_arp_hdr(opcode, sha, tha, sip, tip, &result);
  return result;
}



/* Creates ARP request packet */
static sr_arp_hdr_t
new_arp_req_hdr(struct sr_instance * sr,
                uint32_t tip,
                char *interface)
{
  sr_arp_hdr_t result;
  unsigned short  opcode = arp_op_request;
  struct sr_if *iface = sr_get_interface(sr, interface);

  char *sha = iface->addr;
  char tha[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
  uint32_t sip = iface->ip;

  make_network_arp_hdr(opcode, sha, tha, sip, tip, &result);
  return result;
}






/* Prepares ARP packet in network order from data passed to it and some default
   values. We copy the the ip values directly, because they are passed in network
   order. */
static void
make_network_arp_hdr(unsigned short opcode,
                     char *sha,
                     char *tha,
                     uint32_t sip,
                     uint32_t tip,
                     sr_arp_hdr_t *arp_hdr)
{
  arp_hdr->ar_hrd = htons(arp_hrd_ethernet);
  arp_hdr->ar_pro = htons(ethertype_ip);
  arp_hdr->ar_hln = ETHER_ADDR_LEN;
  arp_hdr->ar_pln = sizeof(uint32_t);
  arp_hdr->ar_op = htons(opcode);
  arp_hdr->ar_sip = sip;
  arp_hdr->ar_tip = tip;
  memcpy(arp_hdr->ar_sha, sha, ETHER_ADDR_LEN);
  memcpy(arp_hdr->ar_tha, tha, ETHER_ADDR_LEN);
}




/* Allocates memory from ethernet frame with ARP data payload.
   Sets up all the relevant values. And returns the pointer to
   allocated data. */
static uint8_t *
new_arp_eth_frame(sr_arp_hdr_t *arp_data)
{
  uint16_t ether_type = htons(ethertype_arp);
  return make_eth_frame(arp_data->ar_tha, arp_data->ar_sha,
                        ether_type, arp_data, sizeof(sr_arp_hdr_t));

}



static sr_arp_hdr_t
make_arp_hdr_host_copy(uint8_t *buf)
{
  sr_arp_hdr_t *arp_hdr = (sr_arp_hdr_t *)(buf);
  sr_arp_hdr_t res;

  res.ar_hrd =  ntohs(arp_hdr->ar_hrd);
  res.ar_pro = ntohs(arp_hdr->ar_pro);
  res.ar_hln = arp_hdr->ar_hln;
  res.ar_pln = arp_hdr->ar_pln;
  res.ar_op  = ntohs(arp_hdr->ar_op);
  res.ar_sip =  ntohl(arp_hdr->ar_sip);
  res.ar_tip = ntohl(arp_hdr->ar_tip);
  memcpy(res.ar_sha, arp_hdr->ar_sha, ETHER_ADDR_LEN);
  memcpy(res.ar_tha, arp_hdr->ar_tha, ETHER_ADDR_LEN);
  return res;
}




void
sr_handle_arpreq(struct sr_instance *sr, struct sr_arpreq *req)
{
  time_t now;
  double diff_t;
  time(&now);

  diff_t = difftime(now, req->sent);
  if (diff_t >= 1.0) {
    if (req->times_sent >= 5) {
      /* send ICMP host unreachable to source addr of
         all pkts waiting on this req */
      struct sr_packet *pkt = req->packets;
      while (pkt) {
        sr_send_icmp_host_unreachable(sr, pkt->buf, pkt->iface);
        pkt = pkt->next;
      }
      
      sr_arpreq_destroy(&sr->cache, req);
    } else {
      /* All the packets waiting on ARP request will be
         targeted to same interface, hence can use interface
         of any packet in ARP request queue. */
      sr_send_arp_request(sr, req->ip, req->packets->iface);
      time(&now);
      req->sent = now;
      req->times_sent++;
    }
  }

}
