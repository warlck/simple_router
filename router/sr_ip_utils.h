#ifndef SR_IP_UTILS_H
#define SR_IP_UTILS_H




#include "sr_router.h"


/* New utils for handling IP packets*/
void sr_handle_ip(struct sr_instance* , uint8_t * , unsigned int , char* );
void sr_ip_pkt_for_us(struct sr_instance* , uint8_t * , unsigned int , char* );
void sr_ip_pkt_not_for_us(struct sr_instance* , uint8_t * , unsigned int , char*);
sr_ip_hdr_t *ip_hdr_from_eth(uint8_t *buf);
int ip_pkt_addressed_to_us(struct sr_instance* sr, sr_ip_hdr_t *iphdr);
sr_ip_hdr_t *make_icmp_ip_datagram(uint32_t ip_src, uint32_t ip_dst, size_t len,
                                   uint8_t *data, uint16_t *ip_id);

uint8_t *new_ip_eth_frame(uint8_t *buf, sr_ip_hdr_t *ip_datagram);

int ip_cksum_is_correct(uint8_t *buf);
size_t ip_hdr_len_with_data(size_t data_len);



#endif