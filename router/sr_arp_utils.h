#ifndef SR_IP_UTILS_H
#define SR_IP_UTILS_H




#include "sr_router.h"
#include "sr_arpcache.h"


void sr_handle_arp(struct sr_instance* , uint8_t * , unsigned int , char* );
void sr_handle_arp_request(struct sr_instance* , sr_arp_hdr_t *, char *);
void sr_handle_arp_reply(struct sr_instance* , sr_arp_hdr_t *, char *);
void sr_send_arp_request(struct sr_instance*, uint32_t , char *);
void sr_handle_arpreq(struct sr_instance *, struct sr_arpreq *);



#endif