#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sr_protocol.h"
#include "sr_utils.h"
#include "sr_if.h"
#include "sr_icmp_utils.h"


static size_t new_icmp_echo_rep_msg(uint8_t *buf, uint8_t **icmp_msg);
static uint8_t* new_icmp_error_msg(uint8_t * buf, uint8_t type, uint8_t code);

static void send_icmp_msg(struct sr_instance *sr, uint8_t *buf,
                          char *interface, uint8_t *icmp_msg, size_t msg_len);
static void prepare_and_send_icmp_err_msg(struct sr_instance * sr,
    uint8_t * buf,
    char *interface,
    uint8_t type,
    uint8_t code);

static size_t
icmp_msg_len(uint8_t *buf)
{
  sr_ip_hdr_t *iphdr = ip_hdr_from_eth(buf);
  sr_icmp_t3_hdr_t *icmp_hdr = icmp_hdr_from_eth(buf);

  /* Get the length of the ICMP message and included data by referring to
     total length field of IP datagram.  */
  size_t len = ntohs(iphdr->ip_len) - sizeof(sr_ip_hdr_t);
  return len;
}



sr_icmp_t3_hdr_t  *
icmp_hdr_from_eth(uint32_t *buf) {
  uint8_t *iphdr = (uint8_t *) ip_hdr_from_eth(buf);
  sr_icmp_t3_hdr_t  *icmp_hdr = (iphdr + sizeof(sr_ip_hdr_t));
  return icmp_hdr;
}


int
icmp_cksum_is_correct(uint8_t *buf)
{
  int res = 0;

  sr_icmp_t3_hdr_t *icmp_hdr = icmp_hdr_from_eth(buf);

  /* Get the length of the ICMP message and included data by referring to
     total length field of IP datagram.  */
  size_t len = icmp_msg_len(buf);
  uint16_t icmp_sum = icmp_hdr->icmp_sum;
  icmp_hdr->icmp_sum = 0;

  if (cksum(icmp_hdr, len) == icmp_sum) {
    res = 1;
  }
  return res;
}



/* Creates ICMP reply message using same data as ICMP request.
   After creating the ICMP, sets the pointer to new data with
   passed in double pointer. This way caller will be able to access
   the created memory after function returns. Function returns the
   length of the allocated memory. */
size_t new_icmp_echo_rep_msg(uint8_t *buf, uint8_t **icmp_msg) {
  sr_icmp_t3_hdr_t *icmp_hdr = icmp_hdr_from_eth(buf);
  size_t len = icmp_msg_len(buf);
  sr_icmp_t3_hdr_t *new_icmp_msg = malloc(len);

  if (new_icmp_msg == NULL) {
    return 0;
  }

  memcpy(new_icmp_msg, icmp_hdr, len);

  new_icmp_msg->icmp_type = icmp_echo_reply;
  new_icmp_msg->icmp_code = 0;
  new_icmp_msg->icmp_sum = 0;
  new_icmp_msg->unused = icmp_hdr->unused;
  new_icmp_msg->next_mtu = icmp_hdr->next_mtu;
  new_icmp_msg->icmp_sum = cksum(new_icmp_msg, len);
  *icmp_msg = new_icmp_msg;

  return len;
}


void
sr_icmp_for_us(struct sr_instance * sr,
               uint8_t * buf,
               char *interface)
{
  if (!icmp_cksum_is_correct(buf)) {
    return;
  }

  if (icmp_hdr_from_eth(buf)->icmp_type != icmp_echo_request) {
    return;
  }

  sr_icmp_t3_hdr_t *icmp_msg;
  size_t msg_len = new_icmp_echo_rep_msg(buf, &icmp_msg);
  /* Could not allocate memory for new ICMP message */
  if (msg_len == 0) {
    return;
  }
  send_icmp_msg(sr, buf, interface, icmp_msg, msg_len);
  free(icmp_msg);
}




void
sr_send_icmp_port_unreachable(struct sr_instance * sr, uint8_t * buf,
                              char *interface)
{
  prepare_and_send_icmp_err_msg(sr, buf, interface, icmp_dest_unreachable,
                                icmp_port_unreachable);
}


void sr_send_icmp_net_unreachable(struct sr_instance * sr, uint8_t * buf,
                                  char *interface)
{
  prepare_and_send_icmp_err_msg(sr, buf, interface, icmp_dest_unreachable,
                                icmp_net_unreachable);
}

void sr_send_icmp_host_unreachable(struct sr_instance * sr, uint8_t * buf,
                                  char *interface)
{
  prepare_and_send_icmp_err_msg(sr, buf, interface, icmp_dest_unreachable,
                                icmp_host_unreachable);
}

void sr_send_icmp_time_exceed(struct sr_instance * sr, uint8_t * buf,
                              char *interface)
{
  prepare_and_send_icmp_err_msg(sr, buf, interface, icmp_time_exceeded, 0);
}



static void
prepare_and_send_icmp_err_msg(struct sr_instance * sr, uint8_t * buf,
                              char *interface, uint8_t type,  uint8_t code)
{
  sr_icmp_t3_hdr_t *icmp_msg = new_icmp_error_msg(buf, type, code);

  if (icmp_msg == NULL) {
    return;
  }

  send_icmp_msg(sr, buf, interface, icmp_msg, sizeof(sr_icmp_t3_hdr_t));
  free(icmp_msg);
}


static uint8_t*
new_icmp_error_msg(uint8_t * buf, uint8_t type, uint8_t code) {
  sr_ip_hdr_t *iphdr = ip_hdr_from_eth(buf);
  sr_icmp_t3_hdr_t *new_icmp_msg = malloc(sizeof(sr_icmp_t3_hdr_t));
  if (new_icmp_msg == NULL) {
    return NULL;
  }

  memset(new_icmp_msg, 0, sizeof(sr_icmp_t3_hdr_t));

  new_icmp_msg->icmp_type = type;
  new_icmp_msg->icmp_code = code;
  new_icmp_msg->icmp_sum = 0;
  new_icmp_msg->unused = 0;
  new_icmp_msg->next_mtu = 0;
  memcpy(&new_icmp_msg->data, iphdr, ICMP_DATA_SIZE);

  new_icmp_msg->icmp_sum = cksum(new_icmp_msg, sizeof(sr_icmp_t3_hdr_t));


  return new_icmp_msg;
}




static void
send_icmp_msg(struct sr_instance * sr, uint8_t * buf,
              char *interface, uint8_t *icmp_msg, size_t msg_len)
{
  sr_ip_hdr_t *iphdr = ip_hdr_from_eth(buf);

  struct sr_if* iface = sr_get_interface(sr, interface);
  uint32_t ip_src = iface->ip;
  /* This is where the IP datagram came from */
  uint32_t ip_dst = iphdr->ip_src;
  sr_ip_hdr_t *new_iphdr = make_icmp_ip_datagram(ip_src, ip_dst,
                           msg_len, icmp_msg, &sr->ip_identification);

  if (new_iphdr == NULL) {
    return;
  }


  uint8_t *icmp_ip_eth_frame  =  new_ip_eth_frame(buf, new_iphdr);
  /* Copied the new_iphdr data int ethernet frame, time to free the memory*/
  free(new_iphdr);

  if (icmp_ip_eth_frame == NULL) {
    return;
  } else {
    size_t len = framelen(ip_hdr_len_with_data(msg_len));
    sr_send_packet(sr, icmp_ip_eth_frame, len, interface);
    free(icmp_ip_eth_frame);
  }
}





