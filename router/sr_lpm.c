#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sr_lpm.h"
#include "sr_ip_utils.h"
#include "sr_eth_utils.h"
#include "sr_protocol.h"
#include "sr_utils.h"
#include "sr_if.h"
#include "sr_rt.h"

static unsigned int count_set_bits(int n);
static int match(struct sr_rt*, uint32_t);


struct sr_rt *
sr_lpm(struct sr_instance * sr, 
    uint8_t *buf) 
{
    /*  for each entry in routing table : 
        'and' the entry IP and subnet mask, 
        'and' the destination IP with subnet mask,
        'xor' the resulting values. If the resulting
        value is 0 then it is match. Count the number 
        of set bits in subnet mask. If resulting count
        is larger then the old match's count, 
        update the lpm to be current route entry. 
    */

    int prefix_len;
    sr_ip_hdr_t *iphdr = ip_hdr_from_eth(buf);
    struct sr_rt *lpm_rt_entry = NULL;
    int longest_prefix_len = -1;

    struct sr_rt* rt_walker = sr->routing_table;

    /* Routing table is empty, no match found */
    if (rt_walker == NULL) {
        return NULL;
    }

    while(rt_walker) {
       if (match(rt_walker, iphdr->ip_dst)) {
         prefix_len = count_set_bits(rt_walker->mask.s_addr);
         if (prefix_len > longest_prefix_len) {
            longest_prefix_len = prefix_len;
            lpm_rt_entry = rt_walker;
         }
       }
       rt_walker = rt_walker->next;
    }

    if (longest_prefix_len == -1) {
       return NULL; 
    }

    return lpm_rt_entry;
}





/* Brian Kernighan’s Algorithm taken from geeksforgeeks.com */
static unsigned int 
count_set_bits(int n)
{
    unsigned int count = 0;
    while (n)
    {
      n &= (n-1) ;
      count++;
    }
    return count;
}


static int 
match(struct sr_rt *rt_walker, uint32_t ip_dst)
{   
    uint32_t mask = rt_walker->mask.s_addr;
    uint32_t dest = rt_walker->dest.s_addr;

    return ((mask & dest) == (mask & ip_dst));
} 