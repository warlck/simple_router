
#ifndef SR_ETH_UTILS_H
#define SR_ETH_UTILS_H

#include <stdint.h>

/* Utils for working with ethernet frames */
uint8_t *make_eth_frame(char *ether_dhost, char *ether_shost, uint16_t ether_type,
                        uint8_t* buf, unsigned int len);
uint32_t framelen(unsigned int len);
void sr_update_eth_addresses(uint8_t * buf, uint8_t *ether_shost,
                             uint8_t *ether_dhost);



#endif